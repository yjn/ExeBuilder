package exebuilder.core;

import exebuilder.data.Description;
import exebuilder.ui.ExeBuilderWindow;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class AppMain extends Application {

  @Override
  public void start(Stage stage) {
    var mainWindow = new ExeBuilderWindow("main");
    mainWindow.setWidth(700);
    mainWindow.setHeight(500);
    var root = ViewLoader.load("mainView");
    var rootScene = new Scene(root);
    mainWindow.setScene(rootScene);
    mainWindow.setResizable(false);
    mainWindow.setTitle(Description.APP_TITLE);
    mainWindow.show();
  }
}
