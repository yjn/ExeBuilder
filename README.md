# ExeBuilder

ExeBuilder 是一款利用 JDK 模块化的特性把jar打包成独立exe的工具，它支持GUI和控制台应用程序的创建。

ExeBuilder 1.0.2 Alpha_setup.exe 安装包下载:

链接：<a href="https://pan.baidu.com/s/1yFKUiP6JcKfU22yx5SMhkw" target="_blank">https://pan.baidu.com/s/1yFKUiP6JcKfU22yx5SMhkw</a>  
提取码: efsq

![](https://gitee.com/qsyan/ExeBuilder/raw/master/images/1.png)  
![](https://gitee.com/qsyan/ExeBuilder/raw/master/images/2.png)  
![](https://gitee.com/qsyan/ExeBuilder/raw/master/images/3.png)  
![](https://gitee.com/qsyan/ExeBuilder/raw/master/images/4.png)

**使用说明：**：  

将项目用 maven-dependency-plugin 和 maven-jar-plugin 打包成可执行jar和lib文件夹，桌面新建文件夹，比如app文件夹，将MainJar（可运行jar）和lib文件夹复制到app文件夹下，
此时，app目录就作为源目录。
安装 ExeBuilder，打开程序按步骤打包即可。

**注意：** 

1、打包后的exe文件所在的目录，自带一个模块化的JRE，可以使用innostep制作成安装包，部署到任意Windows上。  
2、运行本程序并不依赖JDK14,但是要打包exe文件，必须依赖JDK14+。请使用前先下载openjdk14，下载地址：https://download.java.net/openjdk/jdk14/ri/openjdk-14+36_windows-x64_bin.zip  
3、ExeBuilder安装在中文路径下，可能会导致一些问题，请安装在英文路径下。  
4、若要构建JavaFx应用程序，则最低支持JDK11构建的JavaFx应用程序，JDK8默认导出的jar包不包含JavaFx运行依赖（因为JDK8已经包含javafx运行依赖jar），打包exe后会缺少各种运行依赖。

**jdk11下javafx示例：** https://gitee.com/qsyan/jdk11-javafx-maven-demo


**联系作者：**

QQ:331855843  
欢迎加入IDEA微社区QQ群：185441009

===================================================

**版本更新日志：**

1.0.3:  
1、新增内置JDK下载  
2、代码优化


1.0.2：  
1、增加了JDK版本选项  
2、修复了部分BUG
